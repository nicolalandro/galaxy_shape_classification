import pandas as pd
import seaborn
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, recall_score, confusion_matrix
from sklearn.model_selection import train_test_split
import numpy as np


df = pd.read_csv(
    '../../dataset/CALIFA_3_MS_classnum.csv',
    comment='#', header=None,
    names=['CALIFAID', 'REALNAME', 'DBName', 'RA', 'DE', 'hubtyp', 'bar', 'merg']
)

x = []
y = []

for i, row in df.iterrows():
    feature = [row['RA'], row['DE']]
    hubtyp = row['hubtyp']
    x.append(feature)
    y.append(hubtyp)

sns_plot = seaborn.countplot(y)
sns_plot.figure.savefig("img/y_analysis.png")
plt.close()

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33)

clf = KNeighborsClassifier()
clf.fit(x_train, y_train)
predictions = clf.predict(x_test)

cnf_matrix = confusion_matrix(y_test, predictions)
sns_plot = seaborn.heatmap(cnf_matrix, annot=True, center=0)
sns_plot.figure.savefig("img/confusion_matrix.png")
plt.close()

normalized_cnf_matrix = cnf_matrix.astype('float') / cnf_matrix.sum(axis=1)[:, np.newaxis]
sns_plot = seaborn.heatmap(normalized_cnf_matrix, annot=True, center=0, fmt='.1f')
sns_plot.figure.savefig("img/normalized_confusion_matrix.png")
plt.close()

print("decision tree perecision: ", accuracy_score(y_test, predictions))
print("decision tree recal macro: ", recall_score(y_test, predictions, average='macro'))
print("decision tree recal micro: ", recall_score(y_test, predictions, average='micro'))
print("decision tree recal weighted: ", recall_score(y_test, predictions, average='weighted'))
print("decision tree recal none: ", recall_score(y_test, predictions, average=None))
