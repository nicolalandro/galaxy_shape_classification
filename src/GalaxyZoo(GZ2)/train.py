import pandas as pd
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.externals import joblib
from tqdm import tqdm

MAX_SIZE = 667944
LABELS = ['SPIRAL', 'ELLIPTICAL', 'UNCERTAIN']
PATTERN_NAME = ['NVOTE', 'P_EL', 'P_CW', 'P_ACW', 'P_EDGE', 'P_DK', 'P_MG', 'P_CS', 'P_EL_DEBIASED', 'P_CS_DEBIASED']
csv = pd.read_csv('../../dataset/GalaxyZoo1_DR_table2.csv')

y = []
x = []

with tqdm(total=MAX_SIZE) as pbar:
    for index, row in csv.iterrows():
        pbar.update(1)
        row_label = 2
        for i, label in enumerate(LABELS):
            if row[label] == 1:
                row_label = i
        pattern = []
        for p in PATTERN_NAME:
            pattern.append(row[p])

        y.append(row_label)
        x.append(pattern)

clf = ExtraTreesClassifier(n_estimators=100, n_jobs=12, bootstrap=False, min_samples_split=2, random_state=0)
clf.fit(x, y)

joblib.dump(clf, '../../trained_model/decision_tree.pkl')
