import pandas as pd
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score, recall_score
from tqdm import tqdm

MAX_SIZE = 667944
LABELS = ['SPIRAL', 'ELLIPTICAL', 'UNCERTAIN']
PATTERN_NAME = ['NVOTE', 'P_EL', 'P_CW', 'P_ACW', 'P_EDGE', 'P_DK', 'P_MG', 'P_CS', 'P_EL_DEBIASED', 'P_CS_DEBIASED']
csv = pd.read_csv('../../dataset/GalaxyZoo1_DR_table2.csv')

y = []
x = []

with tqdm(total=MAX_SIZE) as pbar:
    for index, row in csv.iterrows():
        pbar.update(1)
        row_label = 2
        for i, label in enumerate(LABELS):
            if row[label] == 1:
                row_label = i
        pattern = []
        for p in PATTERN_NAME:
            pattern.append(row[p])

        y.append(row_label)
        x.append(pattern)

clf = joblib.load('../../trained_model/decision_tree.pkl')
predictions = clf.predict(x)

print("decision tree perecision: ", accuracy_score(y, predictions))
print("decision tree recal macro: ", recall_score(y, predictions, average='macro'))
print("decision tree recal micro: ", recall_score(y, predictions, average='micro'))
print("decision tree recal weighted: ", recall_score(y, predictions, average='weighted'))
print("decision tree recal none: ", recall_score(y, predictions, average=None))
