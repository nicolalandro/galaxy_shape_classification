import pandas as pd
import seaborn
import matplotlib.pyplot as plt
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import accuracy_score, recall_score, confusion_matrix
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import numpy as np

# MAX_SIZE = 667944
size = 2000
LABELS = ['SPIRAL', 'ELLIPTICAL']
PATTERN_NAME = ['NVOTE', 'P_EL', 'P_CW', 'P_ACW', 'P_EDGE', 'P_DK', 'P_MG', 'P_CS', 'P_EL_DEBIASED', 'P_CS_DEBIASED']
csv = pd.read_csv('../../dataset/GalaxyZoo1_DR_table2.csv')[0:size]

y = []
x = []

with tqdm(total=size) as pbar:
    for index, row in csv.iterrows():
        pbar.update(1)
        row_label = 2
        for i, label in enumerate(LABELS):
            if row[label] == 1:
                row_label = i
        pattern = []
        for p in PATTERN_NAME:
            pattern.append(row[p])

        if row_label != 2:
            y.append(row_label)
            x.append(pattern)

sns_plot = seaborn.countplot(y)
sns_plot.figure.savefig("img/y_analysis.png")
plt.close()

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33)

clf = ExtraTreesClassifier(n_estimators=100, n_jobs=12, bootstrap=False, min_samples_split=2, random_state=0)
clf.fit(x_train, y_train)
predictions = clf.predict(x_test)

cnf_matrix = confusion_matrix(y_test, predictions)
sns_plot = seaborn.heatmap(cnf_matrix, annot=True, center=0)
sns_plot.figure.savefig("img/confusion_matrix.png")
plt.close()

normalized_cnf_matrix = cnf_matrix.astype('float') / cnf_matrix.sum(axis=1)[:, np.newaxis]
sns_plot = seaborn.heatmap(normalized_cnf_matrix, annot=True, center=0, fmt='.6f')
sns_plot.figure.savefig("img/normalized_confusion_matrix.png")
plt.close()

print("decision tree perecision: ", accuracy_score(y_test, predictions))
print("decision tree recal macro: ", recall_score(y_test, predictions, average='macro'))
print("decision tree recal micro: ", recall_score(y_test, predictions, average='micro'))
print("decision tree recal weighted: ", recall_score(y_test, predictions, average='weighted'))
print("decision tree recal none: ", recall_score(y_test, predictions, average=None))
