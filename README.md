# Galaxy shape classification
Train AI to known Galaxy shape automatically

## Dataset
### GALAXY ZOO
* [Origin site](https://data.galaxyzoo.org/)
* [Csv file](https://galaxy-zoo-1.s3.amazonaws.com/GalaxyZoo1_DR_table2.csv.gz)
### CALIFA
* [Origin site](http://www.caha.es/CALIFA/public_html/?q=content/dr3-tables#CLASS)
* [Csv file](ftp://ftp.caha.es/CALIFA/docs/DR3/CALIFA_3_MS_classnum.csv)